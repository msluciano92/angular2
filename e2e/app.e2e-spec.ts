import { ArzionPage } from './app.po';

describe('arzion App', function() {
  let page: ArzionPage;

  beforeEach(() => {
    page = new ArzionPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
