export interface UserInterface {
  id: number;
  dni: string;
  username: string;
  password: string;
  image: string;
}