export interface WorkInterface {
  id: number
  title: string;
  description: string;
  state: number;
}