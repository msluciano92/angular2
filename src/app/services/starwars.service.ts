import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Starwars } from '../models';

@Injectable()
export class StarWarsService {
  private heroesUrl = 'http://swapi.co/api/people/';  // URL to web api

  constructor(private http: Http) { }

  getPeople(): Promise<Starwars[]> {
    return this.http
      .get(this.heroesUrl)
      .toPromise()
      .then(response => response.json().results as Starwars[])
      //.then(response => console.log('service loaded', response.json()))
      .catch(this.handleError);
  }

  getPeopleById(id: number): Promise<Starwars> {
    return this.getPeople()
      .then(heroes => heroes.find(hero => hero.id === id));
  }

  save(people: Starwars): Promise<Starwars> {
    if (people.id) {
      return this.put(people);
    }
    return this.post(people);
  }

  delete(hero: Starwars): Promise<Response> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = `${this.heroesUrl}/${hero.id}`;

    return this.http
      .delete(url, { headers: headers })
      .toPromise()
      .catch(this.handleError);
  }

  // Add new Hero
  private post(people: Starwars): Promise<Starwars> {
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http
      .post(this.heroesUrl, JSON.stringify(people), { headers: headers })
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  // Update existing Hero
  private put(people: Starwars): Promise<Starwars> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = `${this.heroesUrl}/${people.id}`;

    return this.http
      .put(url, JSON.stringify(people), { headers: headers })
      .toPromise()
      .then(() => people)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
