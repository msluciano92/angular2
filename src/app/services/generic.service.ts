import {RequestMethod, RequestOptions, Request, Headers, Http} from "@angular/http";
import {GenericModel} from "../interfaces/generic.interface";
/**
 * Created by Arzion on 6/1/2017.
 */


const headers = new Headers({
  'Content-Type': 'application/json'
});


export abstract class GenericService{

  private modelUrl = 'http://localhost:8080/';


  constructor(private http: Http) { }

  abstract getType():string;

  list() {
    //return this.http.get(this.modelUrl)
    return this.http.get("http://192.168.10.139:8080/categories")
      .map(res => {console.log(res); return res.json()});
  }

  httpRequest(method:RequestMethod, url:string = '',  body = undefined){
    let options = new RequestOptions({
      url:`${this.modelUrl}${this.getType()}${url}`,
      method,
      headers,
      body
    })

    return this.http.request(new Request(options));
  }

  save(model:GenericModel){
    if (model.id)
      return this.httpRequest(RequestMethod.Put, `/${model.id}`, JSON.stringify(model))
        .map(() => model);
    else
      return this.httpRequest(RequestMethod.Post, '', JSON.stringify(model))
        .map(res => res.json().data);
  }
}
