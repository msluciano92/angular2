import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {GenericService} from "./generic.service";
/**
 * Created by Arzion on 4/1/2017.
 */

@Injectable()
export class ProductService extends GenericService {

  constructor(private http2: Http) {
    super(http2);
  }

  getType():string{
    return "products";
  }

}
