import {Injectable, Inject} from "@angular/core";
import {GenericService} from "./generic.service";
import {Http} from "@angular/http";
/**
 * Created by Arzion on 6/1/2017.
 */


Injectable()
export class CategoryService extends GenericService{


  constructor(@Inject(Http)private http2: Http) {
    super(http2);
  }

  getType():string{
    return "categories";
  }

}
