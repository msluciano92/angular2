import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Work } from '../models';

@Injectable()
export class WorkService {
  private modelUrl = 'app/works';  // URL to web api

  constructor(private http: Http) { }

  getPeople(): Promise<Work[]> {
    return this.http
      .get(this.modelUrl)
      .toPromise()
      .then(response => response.json().data as Work[])
      .catch(this.handleError);
  }

  getWorkById(id: number): Promise<Work> {
    return this.getPeople()
      .then(works => works.find(work => work.id === id));
  }

  save(work: Work): Promise<Work> {
    if (work.id) {
      return this.put(work);
    }
    return this.post(work);
  }

  delete(work: Work): Promise<Response> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = `${this.modelUrl}/${work.id}`;

    return this.http
      .delete(url, { headers: headers })
      .toPromise()
      .catch(this.handleError);
  }

  // Add new Hero
  private post(work: Work): Promise<Work> {
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http
      .post(this.modelUrl, JSON.stringify(work), { headers: headers })
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  // Update existing Hero
  private put(work: Work): Promise<Work> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = `${this.modelUrl}/${work.id}`;

    return this.http
      .put(url, JSON.stringify(work), { headers: headers })
      .toPromise()
      .then(() => work)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
