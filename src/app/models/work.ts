export class Work {
  id: number;
  title: string;
  description: string;
  state: number;
}