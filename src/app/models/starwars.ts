export class Starwars {
  id: number;
  title: string;
  description: string;
  state: number;
}