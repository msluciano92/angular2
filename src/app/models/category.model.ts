import {GenericModel} from "../interfaces/generic.interface";
/**
 * Created by Arzion on 6/1/2017.
 */

export class CategoryModel implements GenericModel{

  id:number;
  name:string;
  description:string;
}
