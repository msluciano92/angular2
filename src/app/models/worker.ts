export class Worker {
  id: number;
  dni: string;
  username: string;
  password: string;
  cv: string;
  image: string;
}