import {CategoryModel} from "./category.model";
import {GenericModel} from "../interfaces/generic.interface";
/**
 * Created by Arzion on 4/1/2017.
 */
export class ProductModel implements GenericModel{
  id:number;
  title:string;
  price:number;
  description:string;
  category:CategoryModel;
}
