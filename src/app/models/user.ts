export class User {
  id: number;
  dni: string;
  username: string;
  password: string;
  image: string;
}