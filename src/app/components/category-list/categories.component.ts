/**
 * Created by Arzion on 9/1/2017.
 */
import {Component, Input, Output} from "@angular/core";
import {CategoryModel} from "../../models/category.model";
import {EventEmitter} from "@angular/common/src/facade/async";


@Component({
  selector: 'categories-list',
  templateUrl: './categories.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoriesComponent {

  @Input() categories:Array<CategoryModel>;
  @Output() updateCategory=new EventEmitter<CategoryModel>();

}
