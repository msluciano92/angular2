/**
 * Created by Arzion on 9/1/2017.
 */
import {Component, Input, Output} from "@angular/core";
import {CategoryModel} from "../../models/category.model";
import {EventEmitter} from "@angular/common/src/facade/async";
@Component({
  selector: 'category-list',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent{
  @Input() categoryModel: CategoryModel;
  @Output() update=new EventEmitter<CategoryModel>();

}
