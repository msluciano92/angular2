import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { WorkService } from '../../services';
import { WorkInterface } from '../../interfaces';
import { Work } from '../../models';

@Component({
  selector: 'work-add',
  templateUrl: './work-add.component.html',
  styleUrls: ['./work-add.component.css']
})
export class WorkAddComponent {
  constructor(
    private peopleService: WorkService
  ) {}

  workForm: FormGroup;
  @ViewChild('myModal')
  modal: ModalComponent;
  works = [];
  work: Work;
  //Save index of work in works for update
  position = -1

  ngOnInit() {
    this.workForm = new FormGroup({
      title: new FormControl('', [Validators.required, Validators.minLength(2)]),
      description: new FormControl('', [Validators.required]),
      state: new FormControl('', [Validators.required])
    });
  }

  onSubmit({ value, valid }: { value: WorkInterface, valid: boolean }) {
    this.peopleService.save(value)
    .then((work) => {
      if (!this.work.id) {
        this.works.push(work)
      } else {
        this.works[this.position] = work
      }
    })
    this.workForm.reset()
    this.modal.close();
  }

  newWork(work, works, position) {
    this.works = works;
    this.work = work;
    this.position = position;
    if (work.id) {
      this.workForm = new FormGroup({
        title: new FormControl(work.title, [Validators.required, Validators.minLength(2)]),
        description: new FormControl(work.description, [Validators.required]),
        state: new FormControl(work.state, [Validators.required])
      });
    } else {
      this.workForm.reset()
    };
    this.modal.open();
  }
}