/**
 * Created by Arzion on 9/1/2017.
 */

import {Component, ViewChild} from "@angular/core";
import {CategoryService} from "../../services/category.service";
import {CategoryModel} from "../../models/category.model";
import {CategoryFormComponent} from "../category-add/category-add.component";
import {GenericModel} from "../../interfaces/generic.interface";
import {GenericService} from "../../services/generic.service";
import {GenericHandler} from "../../handlers/generic.handler";

@Component({
  providers: [CategoryService],
  selector: 'category-handler',
  templateUrl: './category-handler.component.html',
  styleUrls: ['./category-handler.component.css']
})
export class CategoryHandlerComponent extends GenericHandler{

  categories:Array<CategoryModel> = [];
  @ViewChild(CategoryFormComponent) categoryAdd: CategoryFormComponent;

  constructor(private categoryService: CategoryService) {
    super();
  }
  getChild(){
    return this.categoryAdd;
  }

  getModels():GenericModel[]{
    return this.categories;
  }

  setModels(models:CategoryModel[]):void{
    this.categories=models;
  }

  getService():GenericService{
    return this.categoryService;
  }
}
