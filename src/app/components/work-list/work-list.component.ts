import { Component, ViewChild } from '@angular/core';

import { WorkService } from '../../services';
import { WorkAddComponent } from '../work-add'

@Component({
  selector: 'work-list',
  templateUrl: './work-list.component.html',
  styleUrls: ['./work-list.component.css']
})
export class WorkListComponent {
  constructor(
    private peopleService: WorkService
  ) {}

  @ViewChild(WorkAddComponent) workAdd: WorkAddComponent;

  works = [];

  ngOnInit(): void {
    this.peopleService.getPeople()
    .then(works => this.works = works);
  }

  newWork(): void {
    this.workAdd.newWork({}, this.works, -1);
  }

  updateWork(work, i): void {
    this.workAdd.newWork(work, this.works, i);
  }
}