import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { UserService } from '../../services';
import { UserInterface } from '../../interfaces';
import { User } from '../../models';

@Component({
  selector: 'user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent {
  constructor(
    private userService: UserService
  ) {}

  userForm: FormGroup;
  @ViewChild('myModal')
  modal: ModalComponent;
  users = [];
  user: User;
  //Save index of work in works for update
  position = -1

  ngOnInit() {
    this.userForm = new FormGroup({
      dni: new FormControl('', [Validators.required, Validators.minLength(2)]),
      fullname: new FormControl('', [Validators.required]),
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.required]),
    });
  }

  onSubmit({ value, valid }: { value: UserInterface, valid: boolean }) {
    this.userService.save(value)
    .then((user) => {
      if (!this.user.id) {
        this.users.push(user)
      } else {
        this.users[this.position] = user
      }
    })
    this.userForm.reset()
    this.modal.close();
  }

  newUser(user, users, position) {
    this.users = users;
    this.user = user;
    this.position = position;
    if (user.id) {
      this.userForm = new FormGroup({
        dni: new FormControl(user.dni, [Validators.required, Validators.minLength(2)]),
        fullname: new FormControl('', [Validators.required]),
        username: new FormControl(user.username, [Validators.required]),
        password: new FormControl('', [Validators.required]),
        image: new FormControl(user.image, [Validators.required])
      });
    } else {
      this.userForm.reset()
    };
    this.modal.open();
  }
}