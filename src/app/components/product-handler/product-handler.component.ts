/**
 * Created by Arzion on 5/1/2017.
 */

import {Component, ViewChild} from "@angular/core";
import {ProductService} from "../../services/product.service";
import {ProductModel} from "../../models/product.model";
import {ProductFormComponent} from "../product-add/product-add.component";
import {GenericModel} from "../../interfaces/generic.interface";
import {GenericService} from "../../services/generic.service";
import {GenericHandler} from "../../handlers/generic.handler";

@Component({
  providers: [ProductService],
  selector: 'product-handler',
  templateUrl: './product-handler.component.html',
  styleUrls: ['./product-handler.component.css']
})
export class ProductHandlerComponent extends GenericHandler{

  products:Array<ProductModel> = [];
  @ViewChild(ProductFormComponent) productAdd: ProductFormComponent;

  constructor(private productService: ProductService) {
    super();
  }
  getChild(){
    return this.productAdd;
  }

  getModels():GenericModel[]{
    return this.products;
  }

  setModels(models:ProductModel[]):void{
    this.products=models;
  }

  getService():GenericService{
    return this.productService;
  }
}
