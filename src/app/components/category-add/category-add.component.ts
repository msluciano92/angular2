/**
 * Created by Arzion on 6/1/2017.
 */

import {FormBuilder, FormGroup, FormControl, Validators} from "@angular/forms";
import {Component, Output, ViewChild} from "@angular/core";
import {EventEmitter} from "@angular/forms/src/facade/async";
import {CategoryModel} from "../../models/category.model";
import {ModalComponent} from "ng2-bs3-modal/components/modal";

@Component({
  selector: 'category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryFormComponent{

  @ViewChild('myModal') modal: ModalComponent;
  @Output() formSubmitted=new EventEmitter<CategoryModel>();
  categoryForm:FormGroup;
  nameCtrl:FormControl;
  idCtrl:FormControl;
  descriptionCtrl:FormControl;

  constructor(fb:FormBuilder){
    this.nameCtrl=fb.control('',Validators.compose([Validators.required, Validators.minLength(5)]));
    this.descriptionCtrl=fb.control('');
    this.idCtrl = fb.control('');
    this.categoryForm=fb.group({
      name:this.nameCtrl,
      description:this.descriptionCtrl
    });
  }

  open(category?:CategoryModel) {
    this.categoryForm.reset();
    if (category) this.fillForm(category);
    this.modal.open();
  }

  fillForm(category:CategoryModel){
    this.idCtrl.setValue(category.id);
    this.categoryForm.addControl("id", this.idCtrl);
    this.nameCtrl.setValue(category.name);
    this.descriptionCtrl.setValue(category.description);
  }

  onSubmit(){
    this.formSubmitted.emit(this.categoryForm.value);
    this.modal.close();
  }
}
