import { Component, ViewChild  } from '@angular/core';

import { UserService } from '../../services';
import { UserAddComponent } from '../user-add'

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent {
  constructor(
    private userService: UserService
  ) {}

  @ViewChild(UserAddComponent) userAdd: UserAddComponent;
  
  users = [];

  ngOnInit(): void {
    this.userService.getUsers()
    .then(users => this.users = users);
  }

  newUser(): void {
    this.userAdd.newUser({}, this.users, -1);
  }

  updateUser(user, i): void {
    this.userAdd.newUser(user, this.users, i);
  }
}