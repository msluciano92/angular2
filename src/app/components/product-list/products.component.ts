import {Component, Input, Output} from "@angular/core";
import {ProductModel} from "../../models/product.model";
import {EventEmitter} from "@angular/common/src/facade/async";
/**
 * Created by Arzion on 4/1/2017.
 */


@Component({
  selector: 'products-list',
  templateUrl: './products.component.html',
  styleUrls: ['./product.component.css']
  })
export class ProductsComponent {

  @Input() products:Array<ProductModel>;
  @Output() updateProduct=new EventEmitter<ProductModel>();

}
