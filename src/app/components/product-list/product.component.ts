import {Component, Input, Output} from "@angular/core";
import {ProductModel} from "../../models/product.model";
import {EventEmitter} from "@angular/common/src/facade/async";
/**
 * Created by Arzion on 4/1/2017.
 */
@Component({
  selector: 'product-list',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent{
  @Input() productModel: ProductModel;
  @Output() update=new EventEmitter<ProductModel>();

}
