import {FormBuilder, FormGroup, FormControl, Validators} from "@angular/forms";
import {Component, Output, ViewChild} from "@angular/core";
import {EventEmitter} from "@angular/forms/src/facade/async";
import {ProductModel} from "../../models/product.model";
import {ModalComponent} from "ng2-bs3-modal/components/modal";
/**
 * Created by Arzion on 4/1/2017.
 */
@Component({
  selector: 'product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductFormComponent{

  @ViewChild('myModal') modal: ModalComponent;
  @Output() formSubmitted=new EventEmitter<ProductModel>();
  productForm:FormGroup;
  titleCtrl:FormControl;
  priceCtrl:FormControl;
  idCtrl:FormControl;
  descriptionCtrl:FormControl;

  constructor(fb:FormBuilder){
    this.priceCtrl=fb.control(0,Validators.required);
    this.titleCtrl=fb.control('',Validators.compose([Validators.required, Validators.minLength(5)]));
    this.descriptionCtrl=fb.control('');
    this.idCtrl = fb.control('');
    this.productForm=fb.group({
      title:this.titleCtrl,
      description:this.descriptionCtrl,
      price:this.priceCtrl
    });
  }

  open(product?:ProductModel) {
    this.productForm.reset();
    if (product) this.fillForm(product);
    this.modal.open();
  }

  fillForm(product:ProductModel){
    this.idCtrl.setValue(product.id);
    this.productForm.addControl("id", this.idCtrl);
    this.titleCtrl.setValue(product.title);
    this.priceCtrl.setValue(product.price);
    this.descriptionCtrl.setValue(product.description);
  }

  onSubmit(){
    this.formSubmitted.emit(this.productForm.value);
    this.modal.close();
  }
}
