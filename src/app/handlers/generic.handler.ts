import {GenericService} from "../services/generic.service";
import {GenericModel} from "../interfaces/generic.interface";
/**
 * Created by Arzion on 6/1/2017.
 */

export abstract class GenericHandler{

  abstract getChild();

  abstract getModels():GenericModel[];

  abstract getService():GenericService;

  abstract setModels(models:GenericModel[]):void;

  ngOnInit(): void {
    this.getService().list()
      .subscribe(models => this.setModels(models.data));
  }

  create():void{
    this.getChild().open();
  }

  update(model:GenericModel){
    this.getChild().open(model);
  }

  save(model:GenericModel){
    this.getService().save(model).subscribe(res =>
    {
      this.setModels(this.getModels().reduce(
        (arr, model) => {
          if (res.id !== model.id)
            arr.push(model);
          return arr;
        }, [res]
      ))
    })
  }
}
