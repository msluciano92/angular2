import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './database/in-memory-data.service';

import 'hammerjs';

import { AppComponent } from './components/app';
import { DashboardComponent } from './components/dashboard';
import { WorkListComponent } from './components/work-list';
import { WorkAddComponent } from './components/work-add';
import { UserListComponent } from './components/user-list';
import { UserAddComponent } from './components/user-add';

import { AppRoutingModule, routedComponents } from './app-routing.module';

import { StarWarsService, WorkService, UserService } from './services';
import {ProductsComponent, ProductComponent} from "./components/product-list";
import {ProductHandlerComponent} from "./components/product-handler/product-handler.component";
import {ProductFormComponent} from "./components/product-add/product-add.component";
import {CategoryHandlerComponent} from "./components/category-handler/category-handler.component";
import {CategoryFormComponent} from "./components/category-add/category-add.component";
import {CategoriesComponent} from "./components/category-list/categories.component";
import {CategoryComponent} from "./components/category-list/category.component";
import {CategoryService} from "./services/category.service";

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    WorkListComponent,
    WorkAddComponent,
    UserAddComponent,
    UserListComponent,
    ProductComponent,
    ProductsComponent,
    ProductFormComponent,
    ProductHandlerComponent,
    CategoryComponent,
    CategoriesComponent,
    CategoryFormComponent,
    CategoryHandlerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    MaterialModule.forRoot(),
    ReactiveFormsModule,
    Ng2Bs3ModalModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService, { delay: 600 })
  ],
  providers: [
    StarWarsService,
    WorkService,
    UserService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
