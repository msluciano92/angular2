export class InMemoryDataService {
  createDb() {
    let works = [
    {
      id: 1,
      title: 'Developer',
      description: 'Angular 2',
      state: '1'
    },
    {
      id: 2,
      title: 'Functional',
      description: 'UML required',
      state: '1'
    }
  ];
    let users =[
      {
        dni: "36527384",
        fullname: "Marcos Luciano",
        username: "msluciano",
        password: "mmmmm",
        image: ""
      },
      {
        dni: "36527384",
        fullname: "Marcos Luciano",
        username: "msluciano",
        password: "mmmmm",
        image: ""
      }
    ];
    let products =[
      {
        id:1,
        title: "Merida Bike",
        price: 1033,
        description:"Road 880, Good option for beginners"
      },
      {
        id:2,
        title: "Leaf 4 Dead 2",
        price: 29.99,
        description: "Secuel of the story of Leaf 4 Dead, its the most played online game in steam"
      },
      {
        id:3,
        title: "Rubbo chair",
        price: 75,
        description: "New version from the brand Rubbo, with more confortable position"
      }
    ];
    let categories =[
      {
        id:1,
        name:"Bikes",
        description:"Competition bikes"
      },
      {
        id:2,
        name:"Videogames",
        description:"All the nerd stuff"
      },
      {
        id:3,
        name:"Furniture",
        description:"All the home artifacts"
      }
      ]
    return { works, users, products, categories };
  }
}
