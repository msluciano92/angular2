import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './components/dashboard';
import { WorkListComponent } from './components/work-list';
import { UserListComponent } from './components/user-list';
import {ProductsComponent} from "./components/product-list/products.component";
import {ProductComponent} from "./components/product-list/product.component";
import {ProductHandlerComponent} from "./components/product-handler/product-handler.component";
import {ProductFormComponent} from "./components/product-add/product-add.component";
import {CategoryHandlerComponent} from "./components/category-handler/category-handler.component";


const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'categorylist',
    component: CategoryHandlerComponent
  },
  {
    path: 'productlist',
    component: ProductHandlerComponent
  },
  {
    path: 'worklist',
    component: WorkListComponent
  },
  {
    path: 'userlist',
    component: UserListComponent
  },
  {
    path: '**',
    redirectTo: '/dashboard',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routedComponents = [DashboardComponent, WorkListComponent, UserListComponent, ProductHandlerComponent, CategoryHandlerComponent];
